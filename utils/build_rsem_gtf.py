#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: set softtabstop=4 shiftwidth=4 expandtab:

import argparse

import HTSeq

def main():
    description = ("This script takes a GTF file in input and output another "
                    "GTF file where the 'gene_name' field is moved to the "
                    "'gene_id' field and the 'gene_id' field is moved to "
                    "'gene_id_legacy' field")

    parser = argparse.ArgumentParser(description = description)

    parser.add_argument("input_gtf", nargs = 1, help = "Input GTF file")

    parser.add_argument("output_gtf", nargs = 1, help = "Output GTF file")

    args = parser.parse_args()

    input_gtf_fh = HTSeq.GFF_Reader(args.input_gtf[0])

    with open(args.output_gtf[0], 'w') as output_gtf_fh:
        for gtf_entry in input_gtf_fh:
            gtf_entry.attr['gene_id_legacy'] = gtf_entry.attr['gene_id']
            gtf_entry.attr['gene_id'] = gtf_entry.attr['gene_name']
            output_gtf_fh.write(gtf_entry.get_gff_line())

if __name__ == '__main__':
    main()
