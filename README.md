[![CircleCI](https://circleci.com/bb/lnovara/ngs-tools-docker.svg?style=shield)](https://circleci.com/bb/lnovara/ngs-tools-docker)
[![Docker Build Status](https://img.shields.io/docker/build/lnovara/ngs-tools.svg)](https://hub.docker.com/r/lnovara/ngs-tools/builds/)

# ngs-tools Docker image #

This Docker image bundles some useful tools for the analysis of Next Generation
Sequencing (NGS) data.

Included tools:

  - [BWA v0.7.17](https://github.com/lh3/bwa)
  - [MapSplice v2.2.0](http://www.netlab.uky.edu/p/bioinfo/MapSplice2)
  - [HTSeq v0.9.1](https://github.com/simon-anders/htseq)
  - [Picard v2.17.11](https://github.com/broadinstitute/picard)
  - [RSEM v1.3.0](https://github.com/deweylab/rsem)
  - [Samtools v1.3](https://github.com/samtools/samtools)
  - [UBU v1.2b](https://github.com/mozack/ubu)
